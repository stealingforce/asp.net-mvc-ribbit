﻿using MVC_Ribbit.Data;
using MVC_Ribbit.Models;
using MVC_Ribbit.Services;
using System.Web.Mvc;

namespace MVC_Ribbit.Ui
{
    public abstract class RibbitViewPage : WebViewPage
    {
        protected IContext DataContext;
        public User CurrentUser { get; private set; }
        public IUserService Users { get; }
        public ISecurityService Security { get; }

        protected RibbitViewPage()
        {
            DataContext = new Context();
            Users = new UserService(DataContext);
            Security = new SecurityService(Users);
            CurrentUser = Security.GetCurrentUser();
        }
    }

    public abstract class RibbitViewPage<TModel> : WebViewPage<TModel>
    {
        protected IContext DataContext;
        public User CurrentUser { get; private set; }
        public IUserService Users { get; }
        public ISecurityService Security { get; }

        protected RibbitViewPage()
        {
            DataContext = new Context();
            Users = new UserService(DataContext);
            Security = new SecurityService(Users);
            CurrentUser = Security.GetCurrentUser();
        }
    }
}