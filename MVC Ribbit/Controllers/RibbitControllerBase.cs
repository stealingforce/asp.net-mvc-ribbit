﻿using MVC_Ribbit.Data;
using MVC_Ribbit.Models;
using MVC_Ribbit.Services;
using System.Web.Mvc;

namespace MVC_Ribbit.Controllers
{
    public class RibbitControllerBase : Controller
    {
        protected IContext DataContext;
        public User CurrentUser { get; private set; }
        public IRibbitService Ribbits { get; private set; }
        public IUserService Users { get; private set; }
        public ISecurityService Security { get; private set; }
        public IUserProfileService Profiles { get; private set; }
        public ISubscriptionService Subscriptions { get; private set; }

        public RibbitControllerBase()
        {
            DataContext = new Context();
            Users = new UserService(DataContext);
            Ribbits = new RibbitService(DataContext);
            Security = new SecurityService(Users);
            CurrentUser = Security.GetCurrentUser();
            Profiles = new UserProfileService(DataContext);
            Subscriptions = new SubscriptionService(DataContext);
        }

        protected override void Dispose(bool disposing)
        {
            DataContext?.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GoToReferrer()
        {
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.AbsoluteUri);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}