﻿using System;
using System.Collections.Generic;
using System.IO;
using MVC_Ribbit.ViewModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using Microsoft.Ajax.Utilities;
using MVC_Ribbit.Models;
using MVC_Ribbit.Services;
using Newtonsoft.Json;
using Quartz;
using Rotativa;
using Rotativa.Options;

namespace MVC_Ribbit.Controllers
{
    public class HomeController : RibbitControllerBase
    {

        public ActionResult Index()
        {
            if (!Security.IsAuthenticated)
            {
                return View("Landing", new LoginSignupViewModel());
            }

            var timeline = Ribbits.GetTimelineFor(Security.UserId).ToArray();

            return View("Timeline", timeline);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Follow(string username)
        {
            if (!Security.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            var user = Users.GetBy(username);

            Users.Follow(username, Security.GetCurrentUser());

            return GoToReferrer();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Unfollow(string username)
        {
            if (!Security.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            Users.Unfollow(username, Security.GetCurrentUser());

            return GoToReferrer();
        }

        public new ActionResult Profiles() => View(Users.All(true));

        public ActionResult Followers()
        {
            if (!Security.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            var user = Users.GetAllFor(Security.UserId);

            return View("Followers", new BuddiesViewModel
            {
                User = user,
                Buddies = user.Followers
            });
        }

        public ActionResult Following()
        {
            if (!Security.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            var user = Users.GetAllFor(Security.UserId);

            return View("Following", new BuddiesViewModel
            {
                User = user,
                Buddies = user.Followings
            });
        }

        [HttpGet]
        [ChildActionOnly]
        public ActionResult Create() => PartialView("_CreateRibbitPartial", new CreateRibbitViewModel());

        [HttpPost]
        [ChildActionOnly]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateRibbitViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ribbit = Ribbits.Create(Security.UserId, model.Status);
                Response.Redirect("/");
            }

            return PartialView("_CreateRibbitPartial", model);
        }

        [HttpGet]
        public ActionResult FindAllUsersBy()
        {

            var username = Request.Params["username"];

            return View("FindUsers", Users.GetAllUsersBy(username));


        }

        [HttpGet]
        public ActionResult GetRecentActivity()
        {

            if (!Security.IsAuthenticated)
            {
                return View("Landing", new LoginSignupViewModel());
            }


            var count = Convert.ToInt32(Request.Params["count"]);

            var subscriptionsTimeline = Subscriptions.GetRecentActivityFor(Security.UserId, 10).ToArray();
            var ribbitsTimeline = Ribbits.GetRecentActivityFor(Security.UserId, 10).ToArray();
            var timeline =
                subscriptionsTimeline.Concat(ribbitsTimeline).OrderByDescending(a => a.DateCreated).Take(count < 1 ? 10 : count);

            PreparePdfFiles();
            return View("RecentActivity", timeline);

        }

        public void PreparePdfFiles()
        {
            var users = Users.All(true);

            var deliveryBook = new List<EmailAttachmentViewModel>();

            foreach (var user in users)
            {

                var subscriptionsTimeline = Subscriptions.GetRecentActivityFor(user.Id, 10).ToArray();
                var ribbitsTimeline = Ribbits.GetRecentActivityFor(user.Id, 10).ToArray();
                var timeline =
                    subscriptionsTimeline.Concat(ribbitsTimeline).OrderByDescending(a => a.DateCreated).Take(10);

                if (timeline.Count() >= 10)
                {

                    var actionPdf = new ViewAsPdf("RecentActivity", timeline)
                    {
                        PageSize = Size.A4,
                        PageOrientation = Orientation.Landscape,
                        PageMargins = {Left = 1, Right = 1}
                    };

                    var bytes = actionPdf.BuildPdf(ControllerContext);

                    var pathBuilder = new StringBuilder();
                    pathBuilder.Append("C:/ASP.NET MVC Ribbit/MVC Ribbit/");
                    pathBuilder.Append(user.Username);
                    pathBuilder.Append(".pdf");

                    var path = pathBuilder.ToString();

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    System.IO.File.WriteAllBytes(path, bytes);

                    deliveryBook.Add(new EmailAttachmentViewModel
                    {
                        Email = user.Profile.Email,
                        AttachmentPath = path
                    });

                }
            }

            var deliveryBookJson = JsonConvert.SerializeObject(deliveryBook);
            System.IO.File.WriteAllText("C:/ASP.NET MVC Ribbit/MVC Ribbit/DeliveryBook.json", deliveryBookJson);

        }

    }

}
