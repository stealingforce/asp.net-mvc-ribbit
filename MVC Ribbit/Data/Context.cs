﻿using System;
using System.Data.Entity;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public class Context : IContext
    {
        private readonly DbContext _db;

        public Context(DbContext context = null, IUserRepository users = null,
            IRibbitRepository ribbits = null, IUserProfileRepository profiles = null,
            ISubscriptionRepository subscriptions = null)
        {
            _db = context ?? new RibbitDatabase();
            Users = users ?? new UserRepository(_db, true);
            Ribbits = ribbits ?? new RibbitRepository(_db, true);
            Profiles = profiles ?? new UserProfileRepository(_db, true);
            Subscriptions = subscriptions ?? new SubscriptionRepository(_db, true);
        }

        public IUserRepository Users { get; }

        public IRibbitRepository Ribbits { get; }

        public IUserProfileRepository Profiles { get; }

        public ISubscriptionRepository Subscriptions { get; set; }

        public int SaveChanges() => _db.SaveChanges();

        public void Dispose()
        {
               _db?.Dispose();
        }
    }
}