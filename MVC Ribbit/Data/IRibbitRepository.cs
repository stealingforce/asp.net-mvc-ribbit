﻿using System.Collections.Generic;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public interface IRibbitRepository : IRepository<Ribbit>
    {

        Ribbit GetBy(int id);
        IEnumerable<Ribbit> GetFor(User user);
        void AddFor(Ribbit ribbit, User user);

    }
}
