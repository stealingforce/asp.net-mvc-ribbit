﻿using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public interface IUserProfileRepository : IRepository<UserProfile>
    {

    }
}