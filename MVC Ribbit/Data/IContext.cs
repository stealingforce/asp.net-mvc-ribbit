﻿using System;
namespace MVC_Ribbit.Data
{
    public interface IContext : IDisposable
    {
        IUserRepository Users { get; }
        IRibbitRepository Ribbits { get; }
        IUserProfileRepository Profiles { get; }
        ISubscriptionRepository Subscriptions { get; }

        int SaveChanges();
    }
}
