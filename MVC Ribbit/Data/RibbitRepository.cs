﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public class RibbitRepository : EfRepository<Ribbit>, IRibbitRepository
    {

        public RibbitRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public void AddFor(Ribbit ribbit, User user)
        {
            user.Ribbits.Add(ribbit);

            if (!ShareContext)
            {
                Context.SaveChanges();
            }
        }

        public Ribbit GetBy(int id) => Find(u => u.Id == id);

        public IEnumerable<Ribbit> GetFor(User user) => user.Ribbits.OrderByDescending(r => r.DateCreated);

    }
}