﻿using System.Data.Entity;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public class RibbitDatabase: DbContext
    {

        public RibbitDatabase() : base("RibbitConnection") { }

        public DbSet<User> Users { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Ribbit> Ribbits { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>().
                HasMany(u => u.Followers)
                .WithMany(u => u.Followings)
                .Map(map =>
                {
                    map.MapLeftKey("FollowingId");
                    map.MapRightKey("FollowerId");
                    map.ToTable("Follow");
                });

            modelBuilder.Entity<User>().HasMany(u => u.Ribbits);
            modelBuilder.Entity<User>().HasMany(u => u.Subscriptions);

            base.OnModelCreating(modelBuilder);
        }
    }
}