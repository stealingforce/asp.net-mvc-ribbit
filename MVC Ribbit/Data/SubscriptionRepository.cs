﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public class SubscriptionRepository : EfRepository<Subscription>, ISubscriptionRepository
    {
        public SubscriptionRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public void AddFor(Subscription subscription, User user)
        {
            user.Subscriptions.Add(subscription);

            if (!ShareContext)
            {
                Context.SaveChanges();
            }
        }

        public Subscription GetBy(int id) => Find(u => u.Id == id);

        public IEnumerable<Subscription> GetFor(User user) => user.Subscriptions.OrderByDescending(r => r.DateCreated);
    }
}