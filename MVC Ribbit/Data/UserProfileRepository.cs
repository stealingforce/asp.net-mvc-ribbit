﻿using System.Data.Entity;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public class UserProfileRepository : EfRepository<UserProfile>, IUserProfileRepository
    {
        public UserProfileRepository(DbContext context, bool sharedContext) : base(context, sharedContext) { }
    }
}