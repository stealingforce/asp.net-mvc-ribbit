﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public class UserRepository : EfRepository<User>, IUserRepository
    {

        public UserRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public IQueryable<User> All(bool includeProfile) => includeProfile ? DbSet.Include(u => u.Profile).AsQueryable() : All();

        public void CreateFollower(string username, User follower)
        {
            var user = GetBy(username);
            DbSet.Attach(follower);

            user.Followers.Add(follower);
            user.Subscriptions.Add(new Subscription
            {
                Author = follower,
                AuthorId = user.Id,
                DateCreated = DateTime.Now,
                Target = user
            });

            if (!ShareContext)
            {
                Context.SaveChanges();
            }
        }

        public void DeleteFollower(string username, User follower)
        {
            var user = GetBy(username);
            DbSet.Attach(follower);

            user.Followers.Remove(follower);

            if (!ShareContext)
            {
                Context.SaveChanges();
            }
        }

        public User GetBy(
            string username,
            bool includeProfile = false,
            bool includeRibbits = false,
            bool includeFollowers = false,
            bool includeFollowing = false,
            bool includeSubs = false
            ) => BuildUserQuery(includeProfile, includeRibbits, includeFollowers, includeFollowing, includeSubs).SingleOrDefault(u => u.Username == username);

        public User GetBy(
            int id,
            bool includeProfile = false,
            bool includeRibbits = false,
            bool includeFollowers = false,
            bool includeFollowing = false,
            bool includeSubs = false
            ) => BuildUserQuery(includeProfile, includeRibbits, includeFollowers, includeFollowing, includeSubs).SingleOrDefault(u => u.Id == id);

        public IQueryable<User> GetAllUsersBy(string username) => FindAll(u => u.Username.Contains(username));

        private IQueryable<User> BuildUserQuery(bool includeProfile, bool includeRibbits, bool includeFollowers, bool includeFollowing, bool includeSubs)
        {
            var query = DbSet.AsQueryable();

            if (includeProfile)
            {
                query = DbSet.Include(u => u.Profile);
            }

            if (includeRibbits)
            {
                query = DbSet.Include(u => u.Ribbits);
            }

            if (includeFollowers)
            {
                query = DbSet.Include(u => u.Followers);
            }

            if (includeFollowing)
            {
                query = DbSet.Include(u => u.Followings);
            }

            if (includeSubs)
            {
                query = DbSet.Include(u => u.Subscriptions);
            }

            return query;
        }

    }
}