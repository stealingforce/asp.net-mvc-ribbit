﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Data
{
    public interface ISubscriptionRepository : IRepository<Subscription>
    {
            Subscription GetBy(int id);
            IEnumerable<Subscription> GetFor(User user);
            void AddFor(Subscription ribbit, User user);
    }
}