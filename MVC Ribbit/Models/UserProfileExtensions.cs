﻿using System.Security.Cryptography;
using System.Text;

namespace MVC_Ribbit.Models
{
    public static class UserProfileExtensions
    {
        public static string GetEmailHash(this UserProfile p)
        {
            var email = p.Email.ToLower();

            byte[] hash;
            using (var md5 = MD5.Create())
            {
                hash = md5.ComputeHash(Encoding.UTF8.GetBytes(email));
            }

            var sb = new StringBuilder();

            foreach (var ch in hash)
            {
                sb.Append(ch.ToString("x2"));
            }

            return sb.ToString();

        }
    }
}