﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC_Ribbit.Models
{
    public class Subscription
    {

        public int Id { get; set; }

        public int AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public virtual User Author { get; set; }
        
        public User Target { get; set; }

        public DateTime DateCreated { get; set; }

    }
}