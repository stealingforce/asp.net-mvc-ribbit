﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Ribbit.Models
{
    public class ActivityWrapper
    {

        public Ribbit Ribbit { get; set; }

        public Subscription Subscription { get; set; }

        public DateTime DateCreated { get; set; }

    }
}