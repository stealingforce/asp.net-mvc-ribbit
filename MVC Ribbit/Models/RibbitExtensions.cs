﻿using System;

namespace MVC_Ribbit.Models
{
    public static class RibbitAndActivityExtensions
    {

        public static string FriendlyTimestamp(this Ribbit ribbit)
        {
            var now = DateTime.Now;
            var date = ribbit.DateCreated;
            var span = now - date;

            if (span > TimeSpan.FromHours(24))
            {
                return date.ToString("MMM dd");
            }

            if (span > TimeSpan.FromMinutes(60))
            {
                return string.Format($"{0}h", span.Hours);
            }

            if (span > TimeSpan.FromSeconds(60))
            {
                return string.Format($"{0}m", span.Minutes);
            }

            return "now";
        }

        public static string FriendlyTimestamp(this Subscription subscription)
        {
            var now = DateTime.Now;
            var date = subscription.DateCreated;
            var span = now - date;

            if (span > TimeSpan.FromHours(24))
            {
                return date.ToString("MMM dd");
            }

            if (span > TimeSpan.FromMinutes(60))
            {
                return string.Format($"{0}h", span.Hours);
            }

            if (span > TimeSpan.FromSeconds(60))
            {
                return string.Format($"{0}m", span.Minutes);
            }

            return "now";
        }

    }
}