﻿using MVC_Ribbit.Models;
using System.Collections.Generic;

namespace MVC_Ribbit.ViewModel
{
    public class BuddiesViewModel
    {
        public User User { get; set; }
        public IEnumerable<User> Buddies { get; set; }
    }
}