﻿using MVC_Ribbit.Models;
using System.Collections.Generic;

namespace MVC_Ribbit.ViewModel
{
    public class UserViewModel
    {
        public User User { get; set; }
        public IEnumerable<Ribbit> Ribbits { get; set; }
    }
}