﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Ribbit.ViewModel
{
    public class EmailAttachmentViewModel
    {

        public string Email { get; set; }
        public string AttachmentPath { get; set; }

    }
}