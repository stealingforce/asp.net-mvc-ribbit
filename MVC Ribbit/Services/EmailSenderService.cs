﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using MVC_Ribbit.Controllers;
using MVC_Ribbit.ViewModel;
using Newtonsoft.Json;
using Quartz;
using Rotativa;
using Rotativa.Options;

namespace MVC_Ribbit.Services
{

    public class EmailSenderService : IJob
    {

        public void Execute(IJobExecutionContext context)
        {

            if (!System.IO.File.Exists("C:/ASP.NET MVC Ribbit/MVC Ribbit/DeliveryBook.json"))
            {
                return;
            }
            var serializedDeliveryBook = System.IO.File.ReadAllText("C:/ASP.NET MVC Ribbit/MVC Ribbit/DeliveryBook.json");
            var deliveryBook = JsonConvert.DeserializeObject<List<EmailAttachmentViewModel>>(serializedDeliveryBook);

            foreach (var entry in deliveryBook)
            {

                using (var message = new MailMessage("dr.bazhenoff2017@yandex.ru", entry.Email))
                {

                    message.Subject = "Latest activity";
                    message.Body = "Your followings last activity";
                    message.Attachments.Add(new Attachment(entry.AttachmentPath));

                    using (var client = new SmtpClient
                    {

                        EnableSsl = true,
                        Host = "smtp.yandex.ru",
                        Port = 25,
                        Credentials = new NetworkCredential("dr.bazhenoff2017@yandex.ru", "Shura1Spet")

                    })
                    {
                        client.Send(message);
                    }

                }
            }
        }

    }

}