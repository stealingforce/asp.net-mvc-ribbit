﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Services
{
    public interface ISubscriptionService
    {
        Subscription GetBy(int id);
        Subscription Create(int userId, User target);
        Subscription Create(User user, User target);
        IEnumerable<Subscription> GetTimelineFor(int userId);
        IEnumerable<ActivityWrapper> GetRecentActivityFor(int userId, int count);
    }
}
