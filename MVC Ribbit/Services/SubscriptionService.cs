﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Ribbit.Data;
using MVC_Ribbit.Models;

namespace MVC_Ribbit.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IContext _context;
        private readonly ISubscriptionRepository _subscriptions;

        public SubscriptionService(IContext context)
        {
            _context = context;
            _subscriptions = context.Subscriptions;
        }

        public Subscription GetBy(int id) => _subscriptions.GetBy(id);

        public Subscription Create(User user, User target) => Create(user.Id, target);

        public Subscription Create(int userId, User target)
        {

            var currentUser = _context.Users.GetBy(userId);

            var subscription = new Subscription
            {

                AuthorId = userId,
                Author = currentUser,
                Target = target,
                DateCreated = DateTime.Now

            };

            _subscriptions.Create(subscription);

            _context.SaveChanges();

            return subscription;
        }

        public IEnumerable<Subscription> GetTimelineFor(int userId) =>
            _subscriptions.FindAll(
                r => r.Author.Followers.Any(f => f.Id == userId)
                     || r.AuthorId == userId
            ).OrderByDescending(r => r.DateCreated);

        public IEnumerable<ActivityWrapper> GetRecentActivityFor(int userId, int count)
        {
            var subscriptions =
                _subscriptions.FindAll(r => r.Author.Followers.Any(f => f.Id == userId))
                    .OrderByDescending(r => r.DateCreated);

            foreach (var subscription in subscriptions)
            {
                yield return new ActivityWrapper
                {
                    Subscription = subscription,
                    Ribbit = null,
                    DateCreated = subscription.DateCreated       
                };
            }

        }
    }
}