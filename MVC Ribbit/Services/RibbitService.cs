﻿using MVC_Ribbit.Data;
using MVC_Ribbit.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MVC_Ribbit.Services
{
    public class RibbitService : IRibbitService
    {
        private readonly IContext _context;
        private readonly IRibbitRepository _ribbits;

        public RibbitService(IContext context)
        {
            _context = context;
            _ribbits = context.Ribbits;
        }

        public Ribbit GetBy(int id) => _ribbits.GetBy(id);

        public Ribbit Create(User user, string status) => Create(user.Id, status);

        public Ribbit Create(int userId, string status)
        {
            var ribbit = new Ribbit
            {
                AuthorId = userId,
                Status = status,
                DateCreated = DateTime.Now

            };

            _ribbits.Create(ribbit);

            _context.SaveChanges();

            return ribbit;
        }

        public IEnumerable<Ribbit> GetTimelineFor(int userId) => 
            _ribbits.FindAll(
                r => r.Author.Followers.Any(f => f.Id == userId)
                || r.AuthorId == userId
            ).OrderByDescending(r => r.DateCreated);

        public IEnumerable<ActivityWrapper> GetRecentActivityFor(int userId, int count)
        {

            var ribbits = _ribbits.FindAll(r => r.Author.Followers.Any(f => f.Id == userId)).OrderByDescending(r => r.DateCreated);

            foreach (var ribbit in ribbits)
            {
                yield return new ActivityWrapper {
                    Ribbit = ribbit,
                    Subscription = null,
                    DateCreated = ribbit.DateCreated
                };
            }

        }

    }
}