﻿using MVC_Ribbit.Models;
using System;
using System.Collections.Generic;

namespace MVC_Ribbit.Services
{
    public interface IRibbitService
    {
        Ribbit GetBy(int id);
        Ribbit Create(int userId, string status);
        Ribbit Create(User user, string status);
        IEnumerable<Ribbit> GetTimelineFor(int userId);
        IEnumerable<ActivityWrapper> GetRecentActivityFor(int userId, int count);
    }
}