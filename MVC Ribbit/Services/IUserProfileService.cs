﻿using MVC_Ribbit.Models;
using MVC_Ribbit.ViewModel;

namespace MVC_Ribbit.Services
{
    public interface IUserProfileService
    {
        UserProfile GetBy(int id);
        void Update(EditProfileViewModel model);
    }
}